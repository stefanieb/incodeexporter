package at.aau.IncodeExporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IncodeHTMLParser {

	private String dirName = "/Users/stefanie/Desktop/incode";

	public static void main(String args[]) throws IOException {
		IncodeHTMLParser parser = new IncodeHTMLParser();
		parser.start();

	}

	public void start() throws IOException {

		File[] files = finder(dirName);
		CSVExporter.cleanDirectory(dirName);
		for (File f : files) {
			System.out.println("=================================\nPARSE FILE: " + f.getName()
					+ "\n=================================");
			String text = preprocessFile(f.getPath());
			splitAndParse(text);
		}
	}

	public String preprocessFile(String filename) throws IOException {
		String preprocessedText = readFile(filename, Charset.defaultCharset());
		preprocessedText = removeImages(preprocessedText);
		preprocessedText = cleanText(preprocessedText);
		// System.out.println(preprocessedText);
		return preprocessedText;
	}

	private String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	private String removeImages(String text) {
		return text.replaceAll("<img src=\"\\w+/\\w+\\.\\w+\" alt=\"\\w+\" />", "");
	}

	private String cleanText(String text) {
		String cleanText = text.replaceAll("&nbsp;", "")
				.replaceAll(
						"<td align=left style=\"width: 70px; border-style: solid; border-width: 0.0px 0.0px 0.0px 0.0px; border-color: transparent transparent transparent transparent; padding: 10.0px 2.0px 1.0px 2.0px\">",
						"")
				.replaceAll("<table width=\"1000\" cellpadding=\"0\" cellspacing=\"0\">", "")
				.replaceAll("<th align=left>", "<th>").replaceAll("<font COLOR=\"#5C5858\">", "")
				.replaceAll("</font>", "").replaceAll("</table>", "").replace("", "").replace("&gt;", ">")
				.replace("&lt;", "<");

		return cleanText;
	}

	private String parseLineByLine(String cleanText)
			throws UnsupportedEncodingException, FileNotFoundException, IOException {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("clean.txt"), "utf-8"))) {
			writer.write(cleanText);
		}
		return cleanText;
	}

	private void splitAndParse(String text) {

		String[] split = text.split("<tr>");
		List<String> asList = Arrays.asList(split);
		List<String> parseList = new ArrayList<>();
		parseList.addAll(asList);
		List<String> headerList = parseHeader(parseList.remove(0));
		String smellName = headerList.get(0);
		// System.out.println("-- SMELLNAME: " + smellName);
		// TODO implement import other metrics

		List<Smell> smellList = new ArrayList<>();
		boolean classSmell = isClassSmell(text);
		for (String s : parseList) {

			smellList.addAll(parsePackage(s, smellName, classSmell));

		}
		CSVExporter.export(dirName, smellName, smellList, classSmell);

	}

	private List<Smell> parsePackage(String row, String smellName, boolean classSmell) {

		List<Smell> smellList = new ArrayList<>();

		// package
		Pattern pp = Pattern.compile("((\\w+\\.)+\\w+|<default>)");
		Matcher m_package = pp.matcher(row);

		int pkg_idx = -1;
		String pkg = "";
		if (m_package.find()) {
			pkg_idx = m_package.end();
			pkg = m_package.group().replace(">", "").replace("<", "");
			System.out.println("package: " + pkg);

		}

		// class
		Pattern pc = Pattern.compile("\\w+\\$?\\w+");
		// methods
		Pattern pm = Pattern.compile("\\w+\\((\\w+(\\.\\w+)?\\s\\w+(\\,\\s+)?)*\\)(\\s*:\\s*\\w+)?");
		// severity
		Pattern ps = Pattern.compile("^\\d+$");

		// classes and methods
		String allClum = row.substring(pkg_idx);
		List<String> clumList = new ArrayList<>();
		clumList.addAll(Arrays.asList(allClum.split("<br>")));

		String clazz = "";
		int sevCounter = 0;
		for (String clum : clumList) {

			Matcher mm = pm.matcher(clum);
			if (!clum.equals("") && !clum.equals("</td>") && !clum.equals("<td>")) {
				// method
				if (mm.find()) {
					String methodFull = mm.group();
					int idx = methodFull.indexOf(":");
					String method, retType;
					if (methodFull.contains(":")) {
						method = methodFull.substring(0, idx - 1);
						retType = methodFull.substring(idx + 2);
					} else {
						method = methodFull.trim();
						retType = "Constructor";
					}

					System.out.println("method:" + method);
					Smell s = new Smell();
					s.setSmellName(smellName);
					s.setMethodName(method);
					s.setReturnType(retType);
					s.setClassName(clazz);
					s.setPackageName(pkg);

					smellList.add(s);

				}
				// severity
				else {
					mm = ps.matcher(clum);
					if (mm.find()) {
						Integer severity = new Integer(mm.group());
						smellList.get(sevCounter).setSeverity(severity);
						System.out.println(smellList.get(sevCounter).toString());
						System.out.println("severity: " + severity);
						sevCounter++;
					}
					// class
					else {

						mm = pc.matcher(clum);
						if (mm.find()) {
							clazz = mm.group();
							System.out.println("class:" + clazz);
						}
						if (classSmell) {
							Smell s = new Smell();
							s.setSmellName(smellName);
							s.setClassName(clazz);
							s.setPackageName(pkg);

							smellList.add(s);
						}
					}
				}
			}
		}

		return smellList;
	}

	private Boolean isClassSmell(String row) {
		Pattern pm = Pattern.compile("\\w+\\((\\w+\\.?\\w+\\s\\w+(\\,\\s+)?)*\\)\\s*:\\s*\\w+");
		Matcher mm = pm.matcher(row);
		return !mm.find();
	}

	private List<String> parseHeader(String header) {
		Pattern p = Pattern.compile("<th>(\\w+\\s?\\w*)*</th>");
		Matcher m = p.matcher(header);

		List<String> returnList = new ArrayList<>();

		// name -> ignore
		m.find();
		// smell_name
		m.find();
		returnList.add(m.group().replace("<th>", "").replace("</th>", "").replace("Severity ", ""));

		// possible metrics
		while (m.find()) {
			returnList.add(m.group().replace("<th>", "").replace("</th>", ""));
		}

		return returnList;

	}

	public File[] finder(String dirName) {
		File dir = new File(dirName);

		return dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return filename.endsWith(".html");
			}
		});

	}

}
