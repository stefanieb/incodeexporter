package at.aau.IncodeExporter;

public class Smell {

	private String smellName;
	private Integer severity;
	private String packageName;
	private String className;
	private String methodName;
	private String returnType;

	public String getSmellName() {
		return smellName;
	}

	public void setSmellName(String smellName) {
		this.smellName = smellName;
	}

	public Integer getSeverity() {
		return severity;
	}

	public void setSeverity(Integer severity) {
		this.severity = severity;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	} 

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getFqn() {

		StringBuilder sb = new StringBuilder();
		sb.append(packageName);
		sb.append(".");
		sb.append(className);

		if (methodName != null) {
			sb.append(".");
			sb.append(methodName);
		}

		return sb.toString();

	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("SmellSummary: ");
		sb.append(smellName);
		sb.append(" ");
		sb.append(getFqn());
		
		if(methodName != null && returnType != null){
			sb.append(" > ");
			sb.append(returnType);
		}
		sb.append(": ");
		sb.append(severity);
		
		return sb.toString();

	}

}
